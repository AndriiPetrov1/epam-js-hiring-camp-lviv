var mongoose = require('mongoose');

var orderSchema = mongoose.Schema({
  userId: Number,
  quantity: Number,
  product: String,
  status: {
    enum: ['pending', 'purchased', 'cancelled']
  },
  last_updated: Date
});

orderSchema.pre('save', {query: true}, function(next) {
  // this._id = mongoose.mongo.ObjectId();
  this.last_updated = new Date();
  next();
});

orderSchema.post('save', function(err, doc, next){
  if(err.name !== 'MongoError' || err.code != 11000){
    return next(err);
  }

  var path = 'duplicate key';

  var validationError = new mongoose.Error.ValidationError();
  validationError.errors[path] = validationError.errors[path] || {};
  validationError.errors[path].message = '{0} is expected to be unique.'.replace('{0}', path);
  validationError.errors[path].reason = err.message;
  validationError.errors[path].name = err.name;

  next(validationError);
})

module.exports = mongoose.model('Order', orderSchema); 