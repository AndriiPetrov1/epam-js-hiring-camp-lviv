var mongoose = require('mongoose');

var UserSchema = mongoose.Schema({
  userId: mongoose.Schema.ObjectId,
  name: String,
  status: String,
  last_updated: Date
}, {
  collection: 'users' // In mongodb create users
});



module.exports = mongoose.model('User', userSchema); //mongoDB products