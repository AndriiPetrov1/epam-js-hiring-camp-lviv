var express = require('express');
var router = express.Router();
var faker = require('faker');
var passport = require('passport');

var Order = require('../models/order.model');

var logger = function(req, res, next) {
    console.info(req.params);
    next();
};

// router.use(passport.authenticate('jwt', {session: false}));


router.get('/', function(req, res, next) {
    Order.find({}, function(err, orders){
        if(err) {
            return next(err);
        }
        res.status(200).send(orders);
    });
});

router.get('/:id', function(req, res, next) {
    Product.findOne({_id : req.params.id}, function(err, order) {
        if(err) {
            return next(err);
        }
        res.status(200).send(order);
    });
});

router.post('/', function(req, res, next) {
    var order = new Order(req.body);
    order.save(function(err) {
        if(err) {
            return next(err);
        } 
        res.status(201).send(order);
    });
});

router.delete('/:id', logger, function(req, res, next) {
    Order.deleteOne({_id : req.params.id}, function(err) {
        if(err){
            return next(err);
        }
        res.status(204).send();
    });
});

router.use(function (err, req, res, next) {
    console.log(err);

    if(req.app.get('env') !== 'development'){
        delete err.stack;
    }

    res.status(err.statusCode || 500).json(err);
})

module.exports = router;