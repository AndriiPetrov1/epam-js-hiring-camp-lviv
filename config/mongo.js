var mongoose = require('mongoose');

const dbURI = 'mongodb+srv://User:123@epamhires-js1zg.mongodb.net/test?retryWrites=true&w=majority';

mongoose.connect(dbURI);

mongoose.connection.on('connected', function() {
  console.info("Mongoose connected to: " + dbURI);
});

mongoose.connection.on('error', function() {
  console.info("Mongoose connected error: " + dbURI);
});

module.exports = mongoose;